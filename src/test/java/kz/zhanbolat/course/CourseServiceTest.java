package kz.zhanbolat.course;

import kz.zhanbolat.course.entity.Course;
import kz.zhanbolat.course.service.CourseService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static kz.zhanbolat.course.TestConstant.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class CourseServiceTest {
    @Autowired
    private CourseService courseService;

    @Test
    @DisplayName("CREATE COURSE CASE")
    public void givenExistingCourse_whenCreateCourse_thenThrowException() {
        Course course = new Course();
        course.setId(COURSE_ID);
        course.setName("test");

        assertThrows(IllegalArgumentException.class, () -> courseService.createCourse(course));
    }

    @Test
    @DisplayName("CREATE COURSE CASE")
    public void givenCourse_whenCreateCourse_thenReturnCourseWithId() {
        Course course = new Course();
        course.setName("Test course");

        Course createdCourse = courseService.createCourse(course);
        assertNotNull(createdCourse.getId());
        assertEquals(course.getName(), createdCourse.getName());
    }

    @Test
    @DisplayName("GET COURSES CASE")
    public void givenNull_whenGetCourses_thenReturnCoursesList() {
        List<Course> courses = courseService.getCourses(null);

        assertFalse(courses.isEmpty());
    }

    @Test
    @DisplayName("UPDATE COURSE CASE")
    public void givenCourseWithNullId_whenUpdateCourse_thenThrowException() {
        Course course = new Course();
        course.setName("test");

        assertThrows(Exception.class, () -> courseService.updateCourse(course));
    }

    @Test
    @DisplayName("UPDATE COURSE CASE")
    public void givenNotExistingCourse_whenUpdateCourse_thenThrowException() {
        Course course = new Course();
        course.setId(NOT_EXISTING_COURSE);
        course.setName("test");

        assertThrows(IllegalArgumentException.class, () -> courseService.updateCourse(course));
    }

    @Test
    @DisplayName("GET COURSE CASE")
    public void givenExistingId_whenGetCourse_thenReturnCourse() {
        Course course = courseService.getCourse(COURSE_ID);

        assertNotNull(course);
    }

    @Test
    @DisplayName("GET COURSE CASE")
    public void givenNotExistingId_whenGetCourse_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> courseService.getCourse(NOT_EXISTING_COURSE));
    }

    @Test
    @DisplayName("DELETE COURSE CASE")
    public void givenExistingId_whenDeleteCourse_thenReturnTrue() {
        boolean isDeleted = courseService.deleteCourse(COURSE_DELETE);

        assertTrue(isDeleted);
    }

    @Test
    @DisplayName("DELETE COURSE CASE")
    public void givenNotExistingId_whenDeleteCourse_thenReturnFalse() {
        boolean isDeleted = courseService.deleteCourse(NOT_EXISTING_COURSE);

        assertFalse(isDeleted);
    }

    @Test
    public void givenIds_whenGetCourses_thenReturnListOfOneElement() {
        List<Course> courses = courseService.getCourses(new Long[]{COURSE_ID});
    }
}
