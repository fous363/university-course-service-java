package kz.zhanbolat.course;

public class TestConstant {
    public static final long COURSE_ID = 1L;
    public static final long COURSE_UPDATE = 2L;
    public static final long COURSE_DELETE = 3L;
    public static final long NOT_EXISTING_COURSE = 100L;
}
