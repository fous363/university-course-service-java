package kz.zhanbolat.course;

import kz.zhanbolat.course.entity.Course;
import kz.zhanbolat.course.service.checker.CourseEntityChecker;
import kz.zhanbolat.course.service.checker.CourseEntityCheckerImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class CourseEntityCheckerTest {
    private CourseEntityChecker courseEntityChecker;

    @BeforeEach
    public void init() {
        courseEntityChecker = new CourseEntityCheckerImpl();
    }

    @Test
    public void givenNull_whenCheck_thenThrowException() {
        assertThrows(Exception.class, () -> courseEntityChecker.check(null));
    }

    @Test
    public void givenNullName_whenCheck_thenThrowException() {
        assertThrows(Exception.class, () -> courseEntityChecker.check(new Course()));
    }

    @Test
    public void givenEmptyName_whenCheck_thenThrowException() {
        Course course = new Course();
        course.setName("");
        assertThrows(Exception.class, () -> courseEntityChecker.check(course));
    }
}
