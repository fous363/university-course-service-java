package kz.zhanbolat.course;

import kz.zhanbolat.course.entity.Course;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static kz.zhanbolat.course.TestConstant.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CourseControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        objectMapper = new ObjectMapper();
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("CREATE COURSE CASE")
    public void givenCourse_whenCreateCourse_thenReturnCourse() throws Exception {
        Course course = new Course();
        course.setName("Test course");

        mockMvc.perform(post("/api/course/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").value(course.getName()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("CREATE COURSE CASE")
    public void givenExistingCourse_whenCreateCourse_thenReturnErrorResponse() throws Exception {
        Course course = new Course();
        course.setId(COURSE_ID);
        course.setName("Test course");

        mockMvc.perform(post("/api/course/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @DisplayName("GET COURSES CASE")
    public void givenRequest_whenGetCourses_thenReturnCoursesList() throws Exception {
        mockMvc.perform(get("/api/course/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UPDATE COURSES CASE")
    public void givenExistingCourse_whenUpdateCourse_thenReturnCourse() throws Exception {
        Course course = new Course();
        course.setId(COURSE_UPDATE);
        course.setName("Update the course");

        mockMvc.perform(put("/api/course/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(course.getId()))
                .andExpect(jsonPath("$.name").value(course.getName()));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("UPDATE COURSES CASE")
    public void givenNotExistingCourse_whenUpdateCourse_thenReturnErrorResponse() throws Exception {
        Course course = new Course();
        course.setId(NOT_EXISTING_COURSE);
        course.setName("Update the course");

        mockMvc.perform(put("/api/course/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @DisplayName("GET COURSE CASE")
    public void givenExistingId_whenGetCourse_thenReturnCourse() throws Exception {
        mockMvc.perform(get("/api/course/" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.name").isNotEmpty());
    }

    @Test
    @DisplayName("GET COURSE CASE")
    public void givenNotExistingId_whenGetCourse_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/course/" + NOT_EXISTING_COURSE))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("DELETE COURSE CASE")
    public void givenExistingId_whenDeleteCourse_thenReturnOkStatus() throws Exception {
        mockMvc.perform(delete("/api/course/" + COURSE_DELETE))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    @DisplayName("DELETE COURSE CASE")
    public void givenNotExistingId_whenDeleteCourse_thenReturnInternalServerErrorStatus() throws Exception {
        mockMvc.perform(delete("/api/course/" + NOT_EXISTING_COURSE))
                .andExpect(status().isInternalServerError());
    }

    @Test
    @DisplayName("GET COURSES CASE")
    public void givenIds_whenGetCourses_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/course?ids=" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
