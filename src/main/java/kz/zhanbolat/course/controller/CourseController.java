package kz.zhanbolat.course.controller;

import kz.zhanbolat.course.controller.dto.ErrorResponse;
import kz.zhanbolat.course.entity.Course;
import kz.zhanbolat.course.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/course")
public class CourseController {
    private static final Logger logger = LoggerFactory.getLogger(CourseController.class);
    @Autowired
    private CourseService courseService;

    @PostMapping
    public Course createCourse(@RequestBody Course course) {
        return courseService.createCourse(course);
    }

    @GetMapping
    public List<Course> getCourses(@RequestParam(name = "ids", required = false) Long[] ids) {
        return courseService.getCourses(ids);
    }

    @PutMapping
    public Course updateCourse(@RequestBody Course course) {
        return courseService.updateCourse(course);
    }

    @GetMapping("/{id}")
    public Course getCourse(@PathVariable("id") Long id) {
        return courseService.getCourse(id);
    }

    // return only status, response body is not needed
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCourse(@PathVariable("id") Long id) {
        boolean isDeleted = courseService.deleteCourse(id);
        ResponseEntity.BodyBuilder builder = isDeleted ? ResponseEntity.ok() :
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
        return builder.build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleException(Exception e) {
        logger.error("Caught exception", e);
        return new ErrorResponse(e.getMessage());
    }
}
