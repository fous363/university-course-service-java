package kz.zhanbolat.course.controller.filter;

import kz.zhanbolat.course.controller.dto.AuthorizationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {
    @Value("${authorization.request.url}")
    private String authorizationRequestUrl;
    @Autowired
    private RestTemplate restTemplate;
    @Value("${app.env.test.enabled:false}")
    private boolean isTestEnv;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (!isTestEnv && Objects.nonNull(request.getCookies()) && request.getCookies().length > 0) {
            String cookieHeader = Arrays.stream(request.getCookies())
                    .map(cookie -> cookie.getName() + "=" + cookie.getValue())
                    .collect(Collectors.joining("; "));
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", cookieHeader);
            ResponseEntity<AuthorizationResponse> responseEntity = restTemplate.exchange(authorizationRequestUrl,
                    HttpMethod.POST, new HttpEntity<>(null, requestHeaders), AuthorizationResponse.class);
            AuthorizationResponse authorizationResponse = responseEntity.getBody();
            if (Objects.isNull(authorizationResponse)) {
                response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
                return;
            }
            if (authorizationResponse.isAuthorized()) {
                UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(authorizationResponse.getAuthorizationSubject(),
                        null, authorizationResponse.getAuthorizationRoles().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        filterChain.doFilter(request, response);
    }
}
