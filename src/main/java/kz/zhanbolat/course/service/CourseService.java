package kz.zhanbolat.course.service;

import kz.zhanbolat.course.entity.Course;

import java.util.List;

public interface CourseService {
    Course createCourse(Course course);

    List<Course> getCourses(Long[] ids);

    Course updateCourse(Course course);

    Course getCourse(Long id);

    boolean deleteCourse(Long id);
}
