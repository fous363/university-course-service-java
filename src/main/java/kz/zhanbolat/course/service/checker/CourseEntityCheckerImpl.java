package kz.zhanbolat.course.service.checker;

import kz.zhanbolat.course.entity.Course;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CourseEntityCheckerImpl implements CourseEntityChecker {

    @Override
    public void check(Course course) {
        Objects.requireNonNull(course, "Course cannot be null.");
        if (Objects.requireNonNull(course.getName(), "Course name cannot be null.").isEmpty()) {
            throw new IllegalArgumentException("Course name cannot be empty.");
        }
    }
}
