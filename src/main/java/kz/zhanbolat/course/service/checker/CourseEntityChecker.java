package kz.zhanbolat.course.service.checker;

import kz.zhanbolat.course.entity.Course;

@FunctionalInterface
public interface CourseEntityChecker {

    void check(Course course);
}
