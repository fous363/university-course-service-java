package kz.zhanbolat.course.service;

import kz.zhanbolat.course.entity.Course;
import kz.zhanbolat.course.repository.CourseRepository;
import kz.zhanbolat.course.service.checker.CourseEntityChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseEntityChecker courseEntityChecker;

    @Override
    public Course createCourse(Course course) {
        courseEntityChecker.check(course);

        if (Objects.nonNull(course.getId()) && courseRepository.existsById(course.getId())) {
            throw new IllegalArgumentException("Course already exists with id " + course.getId());
        }

        return courseRepository.save(course);
    }

    @Override
    public List<Course> getCourses(Long[] ids) {
        if (Objects.nonNull(ids) && ids.length > 0) {
            return (List<Course>) courseRepository.findAllById(Arrays.asList(ids));
        }
        return (List<Course>) courseRepository.findAll();
    }

    @Override
    public Course updateCourse(Course course) {
        courseEntityChecker.check(course);
        Objects.requireNonNull(course.getId(), "Course id cannot be null.");

        if (!courseRepository.existsById(course.getId())) {
            throw new IllegalArgumentException("Such course doesn't exist");
        }

        return courseRepository.save(course);
    }

    @Override
    public Course getCourse(Long id) {
        return courseRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("No course with id " + id));
    }

    @Override
    public boolean deleteCourse(Long id) {
        try {
            courseRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
