create table if not exists course(
    id bigserial primary key,
    name varchar(100) unique
);